<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/* Buyers */
Route::resource('buyers','Buyer\BuyerController');
Route::resource('buyers.products','Buyer\BuyerProductController');
Route::resource('buyers.sellers','Buyer\BuyerSellerController');
Route::resource('buyers.categories','Buyer\BuyerCategoryController');
Route::resource('buyers.transactions','Buyer\BuyerTransactionController');
/* Sellers */
Route::resource('sellers','Seller\SellerController');
Route::resource('sellers.transactions','Seller\SellerTransactionController');
Route::resource('sellers.categories','Seller\SellerCategoryController');
Route::resource('sellers.buyers','Seller\SellerBuyerController');
Route::resource('sellers.products','Seller\SellerProductController');
/* Users */
Route::resource('users','User\UserController');
Route::name('verify')->get('users/verify/{token}','User\UserController@verify');
Route::name('resend')->get('users/{user}/resend','User\UserController@resend');


/* Categories */
Route::resource('categories','Category\CategoryController');
Route::resource('categories.products','Category\CategoryProductController');
Route::resource('categories.sellers','Category\CategorySellerController');
Route::resource('categories.transactions','Category\CategoryTransactionController');
Route::resource('categories.buyers','Category\CategoryBuyerController');
/* Products */
Route::resource('products','Product\ProductController');
Route::resource('products.transactions','Product\ProductTransactionController');
Route::resource('products.buyers','Product\ProductBuyerController');
Route::resource('products.categories','Product\ProductCategoryController');
Route::resource('products.buyers.transactions','Product\ProductBuyerTransactionController');

/* Transactions */
Route::resource('transactions','Transaction\TransactionController');
Route::resource('transactions.categories','Transaction\TransactionCategoryController');
Route::resource('transactions.sellers','Transaction\TransactionSellerController');

Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');



